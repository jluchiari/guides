# Guia para remover sintaxe do cérebro
Não da pra armazenar tanta informação na cabeça, pra isso, criei esse monte de guides com estoques de códigos com exemplos e READMEs.

#### DevOps
* [Git](git/)
* [Heroku](heroku/)
* [Docker](docker/)
* [Terraform](terraform/)
* Nginx

#### Linguagens de programação
* [Ruby](ruby/)
  * [Rails](ruby/rails/)
  * [Sinatra](ruby/sinatra/)
* [Javascript](javascript/)
  * [NodeJS](javascript/nodejs/)
  * [VueJS](javascript/vuejs/)
  * [React](javascript/react/)
  * [Angular](javascript/angular/)
* [PHP](php)
  * Laravel
  * [Slim](php/slim/)
* C#
* Python
  * Flask

#### Bancos de dados
* PostgreSQL
* [MongoDB](mongodb/)
* Neo4j
* Elasticsearch
* Redis
* Hadoop
