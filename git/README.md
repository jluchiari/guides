# Guia de uso do git
Sistema de versionamento mais fofo do mundo.

TODO:
- [ ] Adicionar explicação para o gitignore
- [ ] Adicionar explicação para o git stash
- [ ] Revisar explicações e códigos
- [ ] Clone de repositórios
- [ ] Trabalhar com branches
- [ ] Merges
- [ ] Fork
- [ ] Pull requests

#### Instalação
Bem complexo:
```bash
sudo apt update
sudo apt install git
```

#### Configuração do git
Após instalar o git, é necessário realizar algumas configurações, como a seguir:
```bash
git config --global user.name "Seu nome"
git config --global user.email "Seu email"
```

Para exibir as configurações existentes:
```bash
git config --list
```

#### Versionamento
Para inicializar o git em um repositório:
```bash
git init
```

Para visualizar os status das alterações realizadas no repositório:
```bash
git status
```

Para adicionar todos os arquivos ao commit:
```bash
git add -A
# ou
git add .
```

Para adicionar apenas um arquivo ao commit:
```bash
git add path/to/file
```

Para "commitar" as alterações:
```bash
git commit -m "Descreva o que você fez aqui"
```

Para desfazer as últimas alterações e voltar ao último commit
```bash
git checkout -f
```

# Branches
* `git branch` Lista todas as branches.

Para criar uma nova branch:
```bash
git checkout -b new-branch-name
```

Para trocar de branch
```bash
git checkout branch-name
```

Para fazer a mercla na master:
```bash
git checkout master
git merge branch-name
```

Para excluir a branch que já foi mesclada:
```bash
git branch -d branch-name
```

Para excluir uma branch que não será mesclada:
```bash
git branch -D branch-name
```

#### Repositórios online
Para acessar um repositório online de maneira correta, é necessário criar uma chave ssh e adicioná-la na sua conta do github, gitlab ou bitbucket:
```bash
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

Após, você pode clonar um repositório:
```bash
git clone git@<provider>.com:<user>/<repo>.git
```

Ou vincular um repositório a um projeto existente:
```bash
git remote add origin git@<provider>.com:<user>/<repo>.git
```

Para subir as alterações realizadas no repositório:
```bash
git push -u origin <branch> # para a primeira vez que realizar um push
git push # para todas as outras vezes que precisar realizar um push
```

Para sincronizar com o repositório remoto:
```bash
git pull origin <branch> # para a primeira vez que realizar um push/pull
git pull # para todas as outras vezes que precisar realizar um pull
```

Para visualizar as configurações do seu repositório remoto:
```bash
git remote -v
```

Para remover um repositório remoto
```bash
git remote remove origin
```

Para forçar o merge em caso do seguinte erro: `fatal: refusing to merge unrelated histories`
```bash
git pull origin master --allow-unrelated-histories
git merge --allow-unrelated-histories
```
