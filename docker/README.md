# Guia para uso do Docker

### Instalação
É necessário instalar alguns pacotes requeridos pelo docker:
```bash
sudo apt update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

Adicionar a GPG key do docker:
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Adicionar o repositório:
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

Instale o docker:
```bash
sudo apt update
sudo apt install -y docker-ce
```

Para verificar a instalação:
```bash
sudo docker run hello-world
```

Executando docker sem sudo no ubuntu:
```bash
sudo groupadd docker
sudo gpasswd -a $USER docker
newgrp docker
docker run hello-world
```

##### Docker compose
```bash
DOCKER_COMPOSE_VERSION=1.14.0
sudo rm /usr/local/bin/docker-compose
curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > docker-compose
chmod +x docker-compose
sudo mv docker-compose /usr/local/bin
docker-compose --version
```
