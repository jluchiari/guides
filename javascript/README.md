# Guia do JavaScript
Um guia para compreender a linguagem, problemas comuns e soluções comuns.

* [NodeJS](nodejs/)
* [Estrutura de dados](data-structure/)
* [DOM](dom/)
* [Events](events/)
* [Ajax](ajax/)
* [Design patters](design-patterns/)
* [Tratamento de exceções](exceptions/)
* [NPM](npm/)
* [Testes](tests/)
* [Bibliotecas](libs/)
* [JSDoc](jsdoc/)
* [Debug](debug/)
* [ES6](es6/)
* [Code style](https://github.com/airbnb/javascript) - Link externo

#### Frameworks
* [Angular](angular/)
* [React](react/)
* [Vue](vue/)

#### Artigos
* [Programação funcional](https://medium.com/tableless/entendendo-programa%C3%A7%C3%A3o-funcional-em-javascript-de-uma-vez-c676489be08b) - Link externo
* [Modulos](https://stackoverflow.com/questions/16521471/relation-between-commonjs-amd-and-requirejs) - Link externo
