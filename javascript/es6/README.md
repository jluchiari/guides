# Novidades do ES6
Demonstração rápida e simples do ES6.

**Obs:** Ainda falta:
- [ ] Organizar as funcionalidades em seções.
- [ ] Criar um menu para as seções.
- [ ] Melhorar os textos e organizar os exemplos.
- [ ] Testar e revisar os códigos dos exemplos.
- [ ] Pesquisar outras funcionalidades do ES6 [aqui](http://es6-features.org/).
- [ ] Criar arquivos de exemplo

#### let e const
Declaração de variáveis e constantes:
```js
var minha_variavel = "Valor!!!"; // escopo de função
let minha_variavel = "Valor!!!"; // escopo de bloco
const minha_constante = "Valor da constante!!!"; // você não pode reatribuir valores a esta constante
minha_constante = "Outro valor!!!" // isso não funciona, você vai tomar um erro lindo na testa
```

É preferível declarar variávels como const (Éoq?), pois raramente você irá reatribuir algum valor para esta "variável".
Na prática, a `const` pode ser alterada sem problemas, como no exemplo:
```js
const incrementadista = 0;
incrementadista++;
console.log(incrementadista);

// Na prática também não é imutável, você pode reatribuir valores de um objeto (HUEHUEHUE)
const obj = {
  value: 0
}

obj.value = "To trocando o valor e cagando pra constante"; // isso funciona gente
```

#### funções chavosas
Parâmetros com valor default em funções:
```js
function myFunction (arg1, arg2 = 0) {
  console.log(arg1, arg2);
}

myFunction(10); // vai imprimir 10 e 0
```

Parâmetros dinÂÂÂÂmicos (rest parameters)
```js
function myFunction (...numbers) {
  console.log(numbers.length)
}

myFunction(10, 20, 30, 40, 50); // vai imprimir 5 no console
```

Arrow functions (coisa linda, gostosa, tesuda!!!)
```js
// jeito antigo
var sum = function (n1, n2) {
  return n1 + n2;
}

// jeito novo
const sum = (n1, n2) => {
  return n1 * n2;
}

// ou em uma única linha
const sum = (n1, n2) => n1 * n2;

// passando como função anônima
const values = [2, 5, 9, 10, 11, 15];
const array = values.filter((n) => n > 10); // retorna [11, 15]
```

Com arrow functions, o `this` funciona do jeito que deveria:
```js
// javascript tabajara
function Widget() {
  var button = document.getElementById('button');
  button.addEventListener('click', function () {
    this.doSomething(); // o this deveria referenciar o Widget, mas não, ele tem q referênciar a porra do butão
  });
}

// javascript es6
function Widget() {
  const button = document.getElementById('button');
  button.addEventListener('click', () => {
    this.doSomething(); // o this referencia o Widget, como esperado (ou você se acostumou com o jeito errado????????)
  });
}
```

#### destructing
Você pode façar as coisas de um jeito mais simples:
```js
const [a, b] = 1, 2; // o a e b serão atribuidos a seus respectivos valores

const [c, d, ...letters] = 3, 4, 5, 6, 7; // o letters vai ficar com o resto dos valores atribuidos como um array...

const pessoa = {nome: "Julito", idade: 21};
const { nome, idade } = pessoa; // vai "extrair" os atributos da pessoa

// cria 2 variáveis com base no objeto recebido como parâmetro
function myFunction ({ nome, idade }) {
  console.log(nome, idade);
}
myFunction(pessoa)
```

#### Orientação a objetos
```js
class Pessoa {
  constructor (nome) {
    this._nome = nome; // o underscore aqui serve como convenção, para que o atributo seja mantido privado.
  }

  // sintaxe dos getters
  get nome () {
    return this._nome;
  }

  // sintaxe dos setters
  set nome (nome) {
    this._nome = nome;
  }

  // declarando um método normalmente
  metodoLouco () {
    console.log('aweehoo');
  }

  // declarando um método estático
  static metodoEstatico () {
    console.log('Mah oeee');
  }
}

const pessoa = new Pessoa('Julito');
console.log(pessoa.nome); // invocará o método getter
pessoa.nome = 'Julito Luchiari'; // invocará o método setter

Pessoa.metodoEstatico(); // adivinha?
```

Aplicando herança:
```js
class Professor extends Pessoa {
  // rescrevendo o método da classe Pessoa
  metodoLouco () {
    console.log('MySQL >>>>>>> all');
  }
}

class Aluno extends Pessoa {
  metodoLouco () {
    console.log('Não gosto do jorel');
  }
}

const prof = new Professor('Jorel');
const aluno = new Aluno('Arnaldo');
```

#### Modulos
Foda-se o requireJS, commonJS, AMD e tudo mais:
```js
export function sum (a, b) => {
  return a + b;
}

export function mult (a, b) => {
  return a * b;
}
```
No arquivo para usar essa lib cherosa:
```js
import { sum, mult } from 'lib'

sum(1, 2) // 3
mult(1, 2) // 2

// ou...
import * as lib from 'lib'

lib.sum(1, 2) // 3
lib.mult(1, 2) // 2
```

Com default e uma classe:
```js
export default class Pessoa {
  constructor (nome) {
    this._nome = nome;
  }

  get nome () {
    return this._nome;
  }
}
// ~ Em outro arquivo ~
import Pessoa from 'lib'

const pessoa = new Pessoa('Julio');
console.log(pessoa.nome);
```

#### Promises
First class representation of a value that may be made asynchronously and be available in the future.
Evite callback hell:
```js
function msgAfterTimeout (msg, who, timeout) {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(`${msg} Hello ${who}!`), timeout)
    })
}
msgAfterTimeout("", "Foo", 100).then((msg) =>
    msgAfterTimeout(msg, "Bar", 200)
).then((msg) => {
    console.log(`done after 300ms:${msg}`)
})
```

#### Spread operator
Spreading of elements of an iterable collection (like an array or even a string) into both literal elements and individual function parameters.
```js
var params = [ "hello", true, 7 ]
var other = [ 1, 2, ...params ] // [ 1, 2, "hello", true, 7 ]

function f (x, y, ...a) {
    return (x + y) * a.length
}
f(1, 2, ...params) === 9

var str = "foo"
var chars = [ ...str ] // [ "f", "o", "o" ]
```

#### String interpolations
Intuitive expression interpolation for single-line and multi-line strings. (Notice: don't be confused, Template Literals were originally named "Template Strings" in the drafts of the ECMAScript 6 language specification)
```js
var customer = { name: "Foo" }
var card = { amount: 7, product: "Bar", unitprice: 42 }
var message = `Hello ${customer.name},
want to buy ${card.amount} ${card.product} for
a total of ${card.amount * card.unitprice} bucks?`
```

#### Proxy
```js
let target = {
    foo: "Welcome, foo"
}
let proxy = new Proxy(target, {
    get (receiver, name) {
        return name in receiver ? receiver[name] : `Hello, ${name}`
    }
})
proxy.foo   === "Welcome, foo"
proxy.world === "Hello, world"
```

#### Novo for
```js
let fibonacci = {
    [Symbol.iterator]() {
        let pre = 0, cur = 1
        return {
           next () {
               [ pre, cur ] = [ cur, pre + cur ]
               return { done: false, value: cur }
           }
        }
    }
}

for (let n of fibonacci) {
    if (n > 1000)
        break
    console.log(n)
}
```

#### Array find
Retorna somente um elemento, evitando uma merda dessa >>> `[1, 2, 3, 4, 2].filter(function (n) { return n > 3 })[0]`
```js
[ 1, 3, 4, 2 ].find(x => x > 3) // 4
[ 1, 3, 4, 2 ].findIndex(x => x > 3) // 2
```


#### String search
Melhor do que usar index of com comparações ridiculas
```js
"hello".startsWith("ello", 1) // true
"hello".endsWith("hell", 4)   // true
"hello".includes("ell")       // true
"hello".includes("ell", 1)    // true
"hello".includes("ell", 2)    // false
```
