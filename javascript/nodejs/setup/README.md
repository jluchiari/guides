# Setup do NodeJS

* [Download](https://nodejs.org/en/download/)

Verificar a versão:
```bash
node -v
```

### Ubuntu
Para instalar com PPA:
```bash
sudo apt install -y python-software-properties
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt install -y nodejs
```

Para rodar o npm sem sudo:
```bash
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
export PATH=~/.npm-global/bin:$PATH
source ~/.profile
```

### Fedora
```bash
sudo dnf install nodejs
```
