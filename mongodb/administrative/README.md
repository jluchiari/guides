# Administração do MongoDB

### Comandos administrativos
* `mongod` inicializa o servidor.
  * `--dbpath <path>` especifica um diretório para salvar os dados
  * `--directoryperdb` organiza as bases por diretório
  * `--wiredTigerDirectoryForIndexes` separa em diretórios os indices e as collections
* `db.runCommand({<command>})` Executa comandos no mongodb. Lista completa de comandos. [aqui](https://docs.mongodb.com/manual/reference/command/).
* `db.runCommand({ isMaster: 1 }) || db.isMaster()` Verifica se o servidor atual é um _master_ ou um _slave_ de um _replica set_.
* `db.serverStatus()` Mostra toda a informação do servidor atual.
* `db.currentOp()` Mostra todas as operações em progresso.
* `db.killOp(<opid>)` Mata um processo, onde o `<opid>` é o ID do processo em andamento.
* `db.collection.stats()` Verifica o status da _collection_, retornando dados como o nº de documentos e tamanho em disco.
* `db.collection.drop()` Apaga todos os dados da collection.
* `db.runCommand({ listDatabases: 1 })` Lista todos os bancos de dados existentes no servidor e o tamanho total ocupado em disco.
