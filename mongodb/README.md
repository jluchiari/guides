# Guia de uso do MongoDB
O banco de dados mais cheroso da atualidade.

* Lista completa de comandos [aqui](https://docs.mongodb.com/manual/reference/command/) - Documentação oficial

### Tópicos
* [CRUD](crud/)
  * [Consultas](crud/read/)
  * [Inserção](crud/create/)
  * [Atualização](crud/update/)
  * [Exclusão](crud/delete/)
  * [Agregação](crud/aggregation/)
  * [Cursor](crud/cursor/)
  * [Dados geográficos](crud/geographic/)
* [Authentication](auth/)
* [Administrativo](administrative/)
  * [Clusterização](cluster/)
  * [Backup](backup/)
  * [Disaster recovery](disaster-recovery/)
* [Performance](performance/)
* Third party tools
  * [Dockerize](docker/)
  * [Kubernetes](docker/kubernetes/)
  * [Spark](spark/)

### Cenário para uso do MongoDB
USA PRA TUDO!!!

##### BSON
Os documentos do MongoDB seguem o formato BSON (basicamente, JSON binário).
São aceitos os seguintes tipos de dados:
* `Integer` um número inteiro.
* `Float` um número com ponto flutuante.
* `String` uma cadeia de caracteres.
* `Object` um objeto `{}`.
* `Array` um array `[]`.
* `Boolean` `true` ou `false`.
* `Date` uma data no formato ISO.
* `null` um campo sem nenhum valor

##### `_id` field
Todo documento possui um identificador único (`_id`) que por padrão é criado da seguinte maneira:
* 4 caracteres representando a data atual
* 3 caracteres representando o MAC Address
* 2 caracteres com o PID
* 3 caracteres com um contador
* 12 caracteres codificados ao total
