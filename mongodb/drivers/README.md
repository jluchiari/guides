# Drivers de linguagens para uso do MongoDB

### Pure
* Ruby
* Python
* Node.js
* Java
* .NET
* Go
* PHP

### ODM
* Ruby - Mongoid
* Node.js - Mongoose
* Java - Morphia
* PHP - Doctrine
