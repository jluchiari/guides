# Otimização no MongoDB

### Monitorar queries
`db.<collection>.find().explain(<type>)` Visualiza o plano de execução da consulta.
Exemplo:
```js
db.people.find({
  "ssn": "720-38-5636"
}).explain("executionStats");
```


### Indíces
`db.<collection>.getIndexes()` Retorna todos os índices da _collection_.

`db.<collection>.createIndex({ <field>: <direction> })` Cria um índice simples.
Exemplo:
```js
db.people.createIndex({ "ssn": 1 });
```

`db.<collection>.createIndex({ <field>: <direction>, <other_field>: <direction> })` Cria um índice composto.
```js
db.people.createIndex({ "first_name": 1, "last_name": 1 });
```

Para criar índices parciais:
```js
db.restaurants.createIndex({
  "addres.city": 1,
  cuisine: 1
}, {
partialFilterExpression: {
  "stars": { $gte: 3.5 } }
});
```
Nesse caso faz sentido criar indices somente para os restaurantes que possuem uma nota maior ou igual a `3,5`, pois nós sempre estamos procurando ótimos restaurantes.

Para criar índices somente em documentos que contenham determinado campo, evitando indexar null.
```js
db.restaurants.createIndex({
  { stars: 1 },
  { sparse: true }
})
```

### Collations
Permite especificar regras para comparações de strings.
Os possíveis parâmetros são:
```js
{
   locale: <string>,
   caseLevel: <boolean>,
   caseFirst: <string>,
   strength: <int>,
   numericOrdering: <boolean>,
   alternate: <string>,
   maxVariable: <string>,
   backwards: <boolean>
}
```
Exemplo:
```js
db.restaurants.find({"address.city": "ribeirão pires"}).collation({ strength: 1 })
```
