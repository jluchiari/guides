# MongoDB University exercises

#### week 2 extra
Doc example:
```javascript
"awards" : {
    "oscars" : [
        {"award": "bestAnimatedFeature", "result": "won"},
        {"award": "bestMusic", "result": "won"},
        {"award": "bestSoundEditing", "result": "nominated"},
        {"award": "bestScreenplay", "result": "nominated"}
    ],
    "wins" : 56,
    "nominations" : 86,
    "text" : "Won 0 Oscars. I am a fucking God."
}
```

What query would we use in the Mongo shell to return all movies in the video.movieDetails collection that either won or were nominated for a best picture Oscar? You may assume that an award will appear in the oscars array only if the movie won or was nominated. You will probably want to create a little sample data for yourself in order to work this problem.

For this question we are looking for the simplest query that will work. This problem has a very straightforward solution, but you will need to extrapolate a little from some of the information presented in the "Reading Documents" lesson.

Query:
```javascript
db.movieDetails.find({
  "awards.oscars.award": "bestPicture"
})
```


#### hw-2.4-m101 & hw-2.5-m101
Doc example
```javascript
{
	"_id" : ObjectId("569190ca24de1e0ce2dfcd4f"),
	"title" : "Once Upon a Time in the West",
	"year" : 1968,
	"rated" : "PG-13",
	"released" : ISODate("1968-12-21T05:00:00Z"),
	"runtime" : 175,
	"countries" : [
		"Italy",
		"USA",
		"Spain"
	],
	"genres" : [
		"Western"
	],
	"director" : "Sergio Leone",
	"writers" : [
		"Sergio Donati",
		"Sergio Leone",
		"Dario Argento",
		"Bernardo Bertolucci",
		"Sergio Leone"
	],
	"actors" : [
		"Claudia Cardinale",
		"Henry Fonda",
		"Jason Robards",
		"Charles Bronson"
	],
	"plot" : "Epic story of a mysterious stranger with a harmonica who joins forces with a notorious desperado to protect a beautiful widow from a ruthless assassin working for the railroad.",
	"poster" : "http://ia.media-imdb.com/images/M/MV5BMTEyODQzNDkzNjVeQTJeQWpwZ15BbWU4MDgyODk1NDEx._V1_SX300.jpg",
	"imdb" : {
		"id" : "tt0064116",
		"rating" : 8.6,
		"votes" : 201283
	},
	"tomato" : {
		"meter" : 98,
		"image" : "certified",
		"rating" : 9,
		"reviews" : 54,
		"fresh" : 53,
		"consensus" : "A landmark Sergio Leone spaghetti western masterpiece featuring a classic Morricone score.",
		"userMeter" : 95,
		"userRating" : 4.3,
		"userReviews" : 64006
	},
	"metacritic" : 80,
	"awards" : {
		"wins" : 4,
		"nominations" : 5,
		"text" : "4 wins & 5 nominations."
	},
	"type" : "movie"
}
```


Which of the choices below is the title of a movie from the year 2013 that is rated PG-13 and won no awards? Please query the video.movieDetails collection to find the answer.

Resolution
```javascript
db.movieDetails.find({
  rated: "PG-13",
  year: 2013,
  "awards.wins": 0
}, { _id: 0, title: 1 });
```

Using the video.movieDetails collection, how many movies list "Sweden" second in the the list of countries.
```javascript
db.movieDetails.find({
  "countries.1": "Sweden"
}).count()
```


#### hw2.2-m101
Write a program in the language of your choice that will remove the grade of type "homework" with the lowest score for each student from the dataset in the handout. Since each document is one grade, it should remove one document per student.

Doc example
```javascript
{
	"_id" : ObjectId("50906d7fa3c412bb040eb579"),
	"student_id" : 0,
	"type" : "homework",
	"score" : 14.8504576811645
}
```

Resolution:
```javascript
var c = db.grades.find({
  type: "homework"
}).sort({ student_id: 1, score: 1 });

function removeLowestHomeworkScore(cursor) {
  var current = cursor.next();

  while (cursor.hasNext()) {
    var next = cursor.next();

    if (current.student_id == next.student_id && current.score < next.score) {
      db.grades.remove({ _id: current._id });
    }

    current = next
  }
}

```

#### hw2.1-m101
Now it's your turn to analyze the data set. Find all exam scores greater than or equal to 65, and sort those scores from lowest to highest.
What is the student_id of the lowest exam score above 65?

Doc example
```javascript
{
	"_id" : ObjectId("50906d7fa3c412bb040eb579"),
	"student_id" : 0,
	"type" : "homework",
	"score" : 14.8504576811645
}
```

Query
```javascript
db.grades.find({
  score: { $gte: 65 }
}).sort({ score: 1 })
```
