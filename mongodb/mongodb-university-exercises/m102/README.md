# MongoDB University exercises

Doc example:
```javascript
{
	"_id" : ObjectId("507d95d5719dbef170f15c00"),
	"name" : "Phone Service Family Plan",
	"type" : "service",
	"monthly_price" : 90,
	"limits" : {
		"voice" : {
			"units" : "minutes",
			"n" : 1200,
			"over_rate" : 0.05
		},
		"data" : {
			"n" : "unlimited",
			"over_rate" : 0
		},
		"sms" : {
			"n" : "unlimited",
			"over_rate" : 0
		}
	},
	"sales_tax" : true,
	"term_years" : 2
}
```

Load into a shell variable the object corresponding to
```javascript
{ _id : ObjectId("507d95d5719dbef170f15c00") }
```
* Then change term_years to 3 for that document. (And update it in the database.)
* Then change over_rate for sms in limits to 0.01 from 0. Update that too.

Query
```javascript
db.products.update({
  _id : ObjectId("507d95d5719dbef170f15c00")
}, {
  $set: {
    term_years: 3,
    "limits.sms.over_rate": 0.01
  }
});
```

How many products have a voice limit? (That is, have a voice field present in the limits subdocument.)
Input your answer below, (just a number, no other characters).
While you can parse this one by eye, please try to use a query that will do the work of counting it for you.
Just to clarify, the answer should be a number, not a document. There should be no brackets, spaces, quotation marks, etc.

Query
```javascript
db.products.find({
  "limits.voice": { $exists: true }
}).count();
```
