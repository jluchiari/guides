# Uso de cursors no MongoDB

Para declarar um cursor, basta atribuir o resultado de uma consulta a ele.
```javascript
var c = db.collection.find();
```

Para iterar sobre o cursor:
```javascript
var c = db.collection.find();

var doc = function() {
  return c.hasNext() ? c.next() : null
}
```

Para verificar a quantidade de objetos que ainda restam no cursor:
```javascript
var c = db.collection.find();
c.objsLeftInBatch();
```
