# CRUD com MongoDB

* [Create](create/)
* [Read](read/)
* [Update](update/)
* [Delete](delete/)
* [Bulk](#bulk)

### Bulk
Basicamente um Bulk é um acumulo de operações de escrita a serem realizadas em uma unica _collection_.

Com lista não ordenada, será executado os comandos em parelelo, se algum erro ocorrer em alguma operação, continuará executando todas as outras operações:
```javascript
var bulk = db.items.initializeUnorderedBulkOp();
db.items.find(); // should return nothing

bulk.insert({ item: "abc123", defaultQty: 100, status: "A", points: 100 });

db.items.find(); // should return nothing

bulk.insert({ item: "ijk123", defaultQty: 200, status: "A", points: 200 });
bulk.insert({ item: "mop123", defaultQty: 0, status: "P", points: 0 });

bulk.execute();

db.items.find(); // should return 3 documents
```

No caso de lista ordenada, se ocorrer algum erro, irá parar todas as outras operações que seriam executadas:
```javascript
var b = db.items.initializeOrderedBulkOp();

b.find({ item: "abc123" }).remove();
b.find({ item: "mop123" }).remove();
b.find({ item: "ijk123" }).update({ $inc: { points: 1 } });

b.execute();

db.items.find(); // should return only one document
```
