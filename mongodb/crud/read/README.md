# Consultas no MongoDB

### Uso básico
`db.collection.find()` Retorna os todos os documentos na collection, se tiver mais de 10 documentos, retorna um `iterator` que exibe todos os documentos de 10 em 10.
`db.collection.find().pretty()` Formata os resultados do `find`.
`db.collection.findOne()` Retorna um único documento da collection formatado (sem necessidade do uso do `pretty`).
`db.collection.count()` Retorna o número de documentos existentes na collection.
`db.collection.find().count()` Retorna o número de documentos encontrados na consulta.
``

### Filtros
Filtro básico por chave e valor das propriedades do objeto.
```javascript
db.movies.find({
  rated: "PG-13",
  year: 2009
})
```

Uso do "dot notation" para filtrar dados dentro de um objeto.
```javascript
db.movies.find({
  "tomato.metter": 100
})
```

Filtro em arrays:
```javascript
// Exact array match, including order
db.movies.find({
  writters: [
    "Ethan Coen",
    "Joel Coen"
  ]
})

// Match one element in array
db.movies.find({
  actors: "Jeff Bridges"
})

// Match an specific element in array using dot notation
db.movies.find({
  "actors.0": "Jeff Bridges"
})
```

### Projection
Para retornar somente campos específicos de documentos de uma collection, o `find` aceita um segundo parâmetro
```javascript
// Para retornar somente o campo `title`
db.movies.find({ rated: "PG" }, { title: 1 })

// O _id sempre é retornado, para filtrar isso:
db.movies.find({ rated: "PG" }, { _id: 0, title: 1 })
```
Basicamente `field: 1` adiciona um campo ao retorno e `field: 0` remove um campo do retorno.


### Operadores
##### Comparação
* `$eq` "Equal to", compara o campo com um valor específico, ao qual precisa ser igual
* `$ne` "Not equal to", compara o campo com um valor específico, ao qual precisa ser diferente
* `$gt` "Greather than", compara o campo com um valor específico, ao qual precisa ser maior
* `$lt` "Less than", compara o campo com um valor específico, ao qual precisa ser menor
* `$gte` "Greather than or equal to", compara o campo com um valor específico, ao qual precisa ser maior ou igual
* `$lte` "Less than or equal to", compara o campo com um valor específico, ao qual precisa ser menor ou igual
* `$in` "In array", verifica se o valor está entre os valores especificados
* `$nin` "Not in array", verifica se o valor não está entre os valores especificados

```javascript
// Greather than
db.movies.find({
  runtime: { $gt: 90 }
})

// Greather than or equal to and less than or equal to
db.movies.find({
  runtime: { $gte: 90, $lte: 120 }
})

// Not equal
db.movies.find({
  rated: { $ne: "UNRATED" }
})

// Array include
db.movies.find({
  rated: { $in: ["G", "PG"] }
})
```

##### Elementos
* `$exists` Verifica se uma propriedade existe ou não no documento
* `$type` Verifica se uma propriedade é do tipo especificado

```javascript
// exists
db.movies.find({
  "tomato.metter": { $exists: true }
})

// type
db.movies.find({
  _id: { $type: "string" }
})
```

##### Lógicos
* `$or` igual a um valor dentre vários especificados
* `$and` igual a todos os valores especificados
* `$nor` diferente de todos os valores especificados
* `$not` diferente de um valor especificado\\

```javascript
db.movies.find({
  $or: [
    {
      "tomato.metter": { $gt: 95 }
    },
    {
      "metacritic": { $gt: 88 }
    }
  ]
})
```

##### Regex
```javascript
db.movies.find({
  "awards.text": { $regex: /^Won\s.*/ }
})
```

##### Arrays
* `$all` retona todos os documentos que possuem um determinado array que contenha todos os elementos especificados
* `$elemMatch`
* `$size` retorna todos os documentos que possuem um determinado array com um determinado tamanho

```javascript
// Match all
db.movies.find({
  genres: { $all: ["Comedy", "Crime", "Drama"] }
})

// Match size
db.movies.find({
  countries: { $size: 1 }
})

// Match element
db.movies.find({
  boxOffice: { 
    $elemMatch: { country: "USA", revenue: { $lt: 10 } }
  }
})
```
