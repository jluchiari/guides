# Inserção de dados no MongoDB

`db.collection.insert()` Recebe um objeto ou um array de objetos, inserindo um ou mais documentos, retornando o nº de documentos inseridos:
```javascript
db.movies.insert({
  "title": "Rocky",
  "year": 1976,
  "imdb": "tt0075148"
})
```

`db.collection.insertOne()` Insere um único documento, retornando o ID do documento inserido:
```javascript
db.movies.insertOne({
  "title": "Rocky",
  "year": 1976,
  "imdb": "tt0075148"
})
```

`db.collection.insertMany()` Recebe um array de objetos, inserindo N documentos, retornando todos os IDs dos documentos inseridos:
```javascript
db.movies.insertMany([
  {
    "imdb" : "tt0084726",
    "title" : "Star Trek II: The Wrath of Khan",
    "year" : 1982,
    "type" : "movie"
  },
  {
    "imdb" : "tt0796366",
    "title" : "Star Trek",
    "year" : 2009,
    "type" : "movie"
  },
  {
    "imdb" : "tt1408101",
    "title" : "Star Trek Into Darkness",
    "year" : 2013,
    "type" : "movie"
  },
  {
    "imdb" : "tt0117731",
    "title" : "Star Trek: First Contact",
    "year" : 1996,
    "type" : "movie"
  }
]);
```

Para inserir documentos com IDs customizados, não pode se esquecer que a propriedade `_id` é unica e resultará em erro caso exista duplicidades.
Será inserido todos os documentos até que se encontre uma duplicidade e resulte em erro, ignorando os outros documentos no array.
```javascript
db.movies.insertMany([
  {
    "_id" : "tt0084726",
    "title" : "Star Trek II: The Wrath of Khan",
    "year" : 1982,
    "type" : "movie"
  },
  {
    "_id" : "tt0796366",
    "title" : "Star Trek",
    "year" : 2009,
    "type" : "movie"
  },
  {
    "_id" : "tt0084726",
    "title" : "Star Trek II: The Wrath of Khan",
    "year" : 1982,
    "type" : "movie"
  },
  {
    "_id" : "tt1408101",
    "title" : "Star Trek Into Darkness",
    "year" : 2013,
    "type" : "movie"
  },
  {
    "_id" : "tt0117731",
    "title" : "Star Trek: First Contact",
    "year" : 1996,
    "type" : "movie"
  }
]);
```

O comando `insertMany` aceita um segundo parâmetro, falando se a lista será ordenada ou não, fazendo com que continue as inserções mesmo que haja erros de duplicidades. (é claro que não vai inserir os duplicados, só não vai parar com toda a operação)
```javascript
db.movies.insertMany(
  [
    {
      "_id" : "tt0084726",
      "title" : "Star Trek II: The Wrath of Khan",
      "year" : 1982,
      "type" : "movie"
    },
    {
      "_id" : "tt0796366",
      "title" : "Star Trek",
      "year" : 2009,
      "type" : "movie"
    },
    {
      "_id" : "tt0084726",
      "title" : "Star Trek II: The Wrath of Khan",
      "year" : 1982,
      "type" : "movie"
    },
    {
      "_id" : "tt1408101",
      "title" : "Star Trek Into Darkness",
      "year" : 2013,
      "type" : "movie"
    },
    {
      "_id" : "tt0117731",
      "title" : "Star Trek: First Contact",
      "year" : 1996,
      "type" : "movie"
    }
  ],
  {
    "ordered": false
  }
);
```
