```javascript
db.test.remove({ _id: 100 }); // remove document with _id equal to 100
db.test.remove({}); // remove all documents
db.test.drop(); // clean the collection
```
