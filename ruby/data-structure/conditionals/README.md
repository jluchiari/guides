# Estruturas condicionais em Ruby

- [ ] Operadores de comparação (==, !=, &&, ||, !, equals, >, <, >=, <=)
- [ ] Métodos booleanos (empty?, nil?, odd?, even?)
- [x] Sintaxe `if`
- [x] Sintaxe `unless`
- [x] `If` ternário
- [ ] Escopo de variáveis
- [x] Sintaxe `case` `when`
- [ ] Criar arquivos de exemplo
- [ ] Organizar seções
- [ ] Convenções

Estrutura de um if
```ruby
number = 3

if number.odd?
  puts "Ímpar!!!"
end
```

Com else:
```ruby
number = 3

if number.odd?
  puts "Ímpar!!!"
else
  puts "Par!!!"
end
```

Com else if:
```ruby
uf = "SP"

if uf == "SP"
  puts "São Paulo"
elsif uf == "RJ"
  puts "Rio de Janeiro"
else
  puts "Outro estado"
end
```

Estrutura condicional em uma única linha:
```ruby
number = 2

if number.odd? then puts "Ímpar" end # sem else
unless number.odd? then puts "Par" end # com unless
if number.odd? then puts "Ímpar" else puts "Par" end # com else
puts number.odd? ? "Ímpar" : "Par" # if ternário
```

Estrutura condicional para negação (melhor que if !true):
```ruby
(0..5).select { |number| number.even? }

unless filtered.empty?
  puts "Tem coisa nesse array aí!!!"
end
```
_Nota: não use o unless com else, prefira o uso de um if normal, sendo a primeira condição sem nenhuma negação_

Executa um método caso a condição seja verdadeira
```ruby
name = "Júlio"
age = 21

puts age if age.even?
puts name unless name.nil?
```

Sintaxe case when:
```ruby
uf = "SP"
case uf
when "SP" then puts "São Paulo"
when "RJ" then puts "Rio de Janeiro"
when "MG" then puts "Minas Gerais"
else "Outro estado"
end
```
_Nota: por convenção, não se indenta o when_
