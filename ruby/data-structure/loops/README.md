# Estruturas de iteração em Ruby

- [ ] Sintaxe `loop`
- [ ] Sintaxe `begin`
- [ ] Sintaxe `while`
- [ ] Sintaxe `for`
- [ ] Escopo de variáveis
- [ ] Organizar seções
- [ ] Criar arquivos de exemplo
