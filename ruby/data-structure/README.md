# Estrutura de dados em Ruby

* [Strings](strings/)
* [Números](numbers/)
* [Array](arrays/)
* [Hashs](hashs/)
* [Condicionais](conditionals/)
* [Loops](loops/)
* [Métodos/funções](methods/)

## Declaração de variáveis
Basta digitar o nome da variável e associar um valor:
```ruby
variavel = "valor"
```

#### Variáveis globais
Com um cifrão a frente do nome da variável, ela se torna global:
```ruby
$variavel = "valor"
```

## Declaração de constantes
Basta declarar a constante com letras maiúsculas e ta feito!
```ruby
CONSTANTE = "valor constante!!!"
```
