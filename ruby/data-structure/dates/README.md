# Manipulação de datas em Ruby

- [x] Declaração
- [ ] Métodos
- [x] Conversão de strings
- [ ] Cálculos
- [ ] Exibição
- [ ] Organizar seções
- [ ] Criar arquivos de exemplo

#### Time
Para criar uma data:
```ruby
Time.new
```

Para criar uma data específica:
```ruby
Time.new(2017, 07, 17)
```

Para converter a data em unix time:
```ruby
timestamp = Time.new.to_i
```

Para utilizar as datas sem timezone:
```ruby
Time.utc
```

Para verificar se uma data está entre 2 datas:
```ruby
start_date = Time.new(2017, 01, 01)
end_date = Time.new(2017, 06, 30)

Time.new.between?(start_date, end_date)
```

Converter para a classe date:
```ruby
Time.new.to_date
```

#### Date
Para converter uma string em data:
```ruby
Date.strptime("30/06/1996", "%d/%m/%Y")
```
**Mais informações [aqui](https://ruby-doc.org/stdlib-2.1.1/libdoc/date/rdoc/Date.html)**


Converter uma data em string:
```ruby
Date.strftime("%d/%m/%Y")
```

Converter para a classe time
```ruby
Date.strptime("30/06/1996", "%d/%m/%Y").to_time
```

#### DateTime
Criar um datetime:
```ruby
DateTime.new(2017, 01, 30, 15, 20, 20)
````
