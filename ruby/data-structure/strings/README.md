# Manipulação de strings em Ruby

- [ ] Declaração
- [ ] Concatenação
- [ ] Métodos
- [ ] Regex
- [ ] Criar arquivos de exemplo
- [ ] Organizar seções


Para utilizar uma expressão regular em uma string:
```ruby
str = "algum valor para esta string"
str.scan(#expressão regular vai aqui)
```

Pode se utilizar também desta forma:
```ruby
/abc/ =~ "abcdefgh" # retorna null caso não encontre nada ou a posição em que a string se inicia
```

Para retornar os valores encontrados em uma expressão regular
```ruby
x = /(^.*)(#)(.*)/.match('def myMethod # This is a very nice method')
puts x.captures
```

Alguns métodos de string também podem receber como parâmetro, expressões regulares:
```ruby
s.split(//)
s.sub(//)
s.gsub(//)
```
