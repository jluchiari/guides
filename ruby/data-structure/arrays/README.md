# Manipulação de arrays em Ruby

- [x] Declaração
- [x] Métodos
- [x] Iteração
- [ ] Pesquisar mais métodos para arrays
- [ ] Organizar as seções
- [ ] Criar arquivos de exemplo

#### Declaração
Para declarar um array e acessar algum elemento:
```ruby
arrayname = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
puts arrayname[0] # imprime 1
```

Para acessar o índice de um array de trás para frente, basta utilizar indices negativos:
```ruby
arrayname = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
puts arrayname[-1] # imprime 10
```

Para converter dados em array
```ruby
someobject.to_a
```

Criando arrays dinâmicamente:
```ruby
(0..25).to_a # cria um array com os elementos de `0` a `25`
(0..99).to_a.shuffle! # cria um array com os elementos de `0` a `99` em ordem aleatória
```

#### Métodos
`arrayname.empty?`: Verifica se o array está vazio

`arrayname.length`: Retorna a quantidade de itens no array

`arrayname.include?(itemname)`: Verifica se o array possui o item passado como parâmetro

`arrayname.reverse`: Retorna um novo array com as posições revertidas

`arrayname.shuffle`: Retorna um novo array com as posições alteradas randomicamente

`arrayname.push(30)`: Adiciona um novo elemento ao final do array

`arrayname << 25`: Conhecido como `shovel operator` tem a mesma função do push

`arrayname.insert(1, 30)`: Adiciona um novo elemento (30) no indice especificado (1)

`arrayname.unshift("someelement")`: Adiciona um novo elemento no começo do array

`arrayname.pop`: Remove o ultimo elemento e retorna o elemento removido

`arrayname.shift`: Remove o primeiro elemento e retorna o elemento removido

`arrayname.delete_at(3)`: Remove o elemento no indice especificado (3)

`arrayname.delete(5)`: Remove todos os elementos iguais ao especificado (5)

`arrayname.uniq`: Retorna um novo array sem valores duplicados

`arrayname.sort`: Retorna um novo array reordenado de forma crescente

`arrayname.take(3)`: Retorna os 3 primeiros elementos de um array

`arrayname.drop(3)`: Retorna todos os elementos do array, exceto os 3 primeiros

Para alterar o array original, que está invocando o método, use os bang methods (!), como no exemplo a seguir:
```ruby
arrayname.sort! # reordena o array de forma crescente
arrayname.reverse! # reverte o array
arrayname.uniq! # remove elementos duplicados
```

Para converter em uma string (não tem bang method):
```ruby
arrayname.join # retorna uma string com os elementos do array
arrayname.join(",") # retorna uma string com os elementos do array separados por virgula
```

#### Iteração
Iteração simples em um array:
```ruby
arrayname.each { |elem| puts elem } # irá imprimir individualmente cada elemento do array
```

Para filtrar dados de um array:
```ruby
arrayname.select { |number| number.odd? } # irá retornar os elementos do array que correspondem a condição
arrayname.reject { |number| number.odd? } # irá retornar os elementos do array que não correspondem a condição
```

Iteração para remover elementos:
```ruby
arrayname.drop_while { |a| a > 1 } # remove todos os elementos até que a condição seja falsa
arrayname.delete_if {|a| a < 2} # remove todos os elementos que satisfazem a condição
arr.keep_if {|a| a < 4} # remove todos os elementos que não satisfazem a condição
```
