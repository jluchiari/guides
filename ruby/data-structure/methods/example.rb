a = Proc.new { |x| x = x * 10; puts x }
b = lambda { |x| x = x * 10; puts x }
c = proc { |x| x.capitalize! }

a.call(10)
b.call(20)
puts c.call("hue")

# sintaxe yield
def my_method (*args)
  puts "Executando o método"
  yield(args)
end

my_method(1,2,3) { |elements| puts elements.to_s }
