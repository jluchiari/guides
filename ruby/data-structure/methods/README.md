# Declaração e utilização de métodos/funções em Ruby

- [ ] Declaração de métodos/funções
- [ ] Parâmetros
- [ ] Uso de métodos e funções
- [ ] Métodos que retornam booleanos
- [ ] Bang methods
- [ ] Sintaxe `lambda`
- [ ] Sintaxe `proc`
- [ ] Sintaxe `yield`
- [ ] Criar arquivos de exemplo
- [ ] Organizar seções

Sintaxe `Proc` e `lambda`:
```ruby
a = Proc.new { |x| x = x * 10; puts x }
b = lambda { |x| x = x * 10; puts x }
c = proc { |x| x.capitalize! }

puts a(10)
puts b(20)
puts c("hue")
```

Sintaxe `loop`:
```ruby
3.times { |i| puts i }

0..2.each { |i| puts i }

3.times do |i|
  #alguma lógica multilinha maluca
end
```

Sintaxe `yield`:
```ruby
def my_method (*args)
  puts "Executando o método"
  yield(args)
end

my_method(1,2,3) do |elements|
  puts elements.to_s
end
```
