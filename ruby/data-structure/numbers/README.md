# Manipulação de números em Ruby

- [x] Declaração
- [ ] Métodos
- [x] Operadores
- [ ] Exibição
- [ ] Organizar seções
- [ ] Criar arquivos de exemplo
- [ ] Biblioteca Math

Operadores matemáticos:
```ruby
1 + 2
1 * 2
1 / 2
1 - 2
1 % 2
```

Para indicar que um número é um decimal, ao invés de um inteiro, inclua um . no número:
```ruby
number = 20 # inteiro
number = 20.0 # float

# ou
number = 20.to_f # converter para float
number = 20.0.to_i # converter para inteiro
```

Métodos disponívels:
`number.odd?` - verifica se o número é ímpar;
`number.even?` - verifica se o número é par;
`number.between?(5, 10)` - verifica se o número está entre 5 e 10;

Gerar números aleatórios:
```ruby
number = rand(10) # gera um número entre 0 e 10
```

Para converter uma string em número:
```ruby
"5".to_i
```
