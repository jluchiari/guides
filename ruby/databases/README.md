# Manipulando bancos de dados com Ruby

#### Banco relacional
* MariaDB
* MySQL
* [PostgreSQL](postgres)
* SQL Server
* Oracle SQL
* ORM (Object-Relational Mapping)

#### Banco de documentos
* Mongo
* Cassandra

#### Banco de grafos
* Neo4j

#### Search engine
* Elastic Search

#### Real time
* Rethink
