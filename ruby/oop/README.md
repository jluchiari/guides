# Orientação a objetos em Ruby

- [ ] Classes
- [ ] Métodos
- [ ] Atributos
- [ ] Construtores
- [ ] Encapsulamento
- [ ] Herança
- [ ] Módulos
- [ ] Singleton
- [ ] Override
- [ ] Organizar seções
- [ ] Criar arquivos de exemplo

Mostra todos os simbolos:
```ruby
puts Symbol.all_symbols.sort
```

Variáveis globais
```ruby
$my_variable = "some value"
```


Sintaxe para criação de funções/métodos:
```ruby
def my_function
  #lógica
end

def my_function_with_args (arg1, arg2)
  #lógica
end

# aqui você pode passar quantos parâmetros quiser, que será criado um array automágicamente
def my_function_with_array_args (*args)
  #lógica
end

# sempre use os parenteses para não confundir com variáveis
my_function()

my_function_with_args("1", "2")

# os parametros vão virar tudo um array
my_function_with_array_args(1, 70.6, "huehue", "k", {a: 'b', foo: 'bar'})
```

Sintaxe para declaração de classes:
```ruby
class #nome da classe
  # metodos e atributos vão aqui!!!
end
```

Sintaxe para declaração de métodos e variáveis de instância:
```ruby
class #nome da classe

  @#nome da variável = #valor da variável

  def #nome do método
    #lógica
  end

end
```

Sintaxe para declaração de métodos e variáveis da classe:
```ruby
class #nome da classe

  @@#nome da variável = #valor da variável

  def self.#nome do método
    #lógica
  end

  #pode usar o nome da classe ao invés de self
  def #nome da classe.#nome do método
    #lógica
  end

end
```
_Nota: Você pode acessar variáveis de instância em métodos da classe_

Sintaxe para declaração de construtores:
```ruby
class #nome da classe

  def initialize
    #lógica
  end

end
```

Sintaxe para criação de singleton method:
```ruby
class MyClass
  # alguns métodos...
end

obj = MyClass.new

def obj.new_method
  #lógica
end

obj.new_method()
```
_Nota: o novo método criado para o objeto será visto apenas por ele, não é compartilhado com outras instâncias ou com a classe_

Sintaxe para criação de singleton class:
```ruby
class MyClass
  # alguns métodos...
end

obj = MyClass.new

class << obj
  def new_method
    #lógica
  end
end

obj.new_method()
```
_Nota: o novo método criado para o objeto será visto apenas por ele, não é compartilhado com outras instâncias ou com a classe_

Sintaxe para encapsulamento:
```ruby
class MyClass
  attr_reader :readonly_attr # get
  attr_writer :writeonly_attr # set
  attr_accessor :public_attr # get e set

  def initialize
    @readonly_attr = 1
    @writeonly_attr = 2
    @public_attr = 3
    @private_attr = 4 # não estará visivel para ninguém
  end

  private
    def my_private_method
      puts "Método privado"
    end

  protected
    def my_protected_method
      puts "Método protegido"
    end

  public
    def my_public_method
      puts "Método público"
    end
end

my_obj = MyClass.new
puts my_obj.readonly_attr
puts my_obj.writeonly_attr # não funciona
puts my_obj.public_attr
puts my_obj.private_attr # não funciona

puts my_obj.my_private_method # não funciona
puts my_obj.my_protected_method # não funciona
puts my_obj.my_public_method
```

Sintaxe Bang methods (!):
```ruby
nome = "Júlio"
puts nome.reverse #retorna o nome ao contrário, mas não altera o valor do objeto
puts nome #mostra o valor original da variável

puts nome.reverse! #retorna o nome ao contrário e o valor do objeto
puts nome #mostra o valor revertido do objeto
```

Sintaxe para criação de um module
```ruby
module MyModule
  #some methods and attributes
end
```

Para incluir um modulo em uma classe:
```ruby
class MyClass
  include MyModule

  #some class methods
end
```

Você pode usar o `alias` caso os modulos tenham conflito nos nomes dos métodos
```ruby
module MyFirstModule
  def same_name_method
    # lógica
  end
  alias first_same_name_method same_name_method
end

module MySecondModule
  def same_name_method
    # lógica
  end
  alias second_same_name_method same_name_method
end

class MyClass
  # A decisão de qual método de qual modulo com o mesmo nome será executado, é pela ordem de inclusão na classe
  include MyFirstModule
  include MySecondModule

  def initialize
    # lógica

    same_name_method
  end
end

obj = MyClass.new
obj.same_name_method  #executará o modulo MySecondModule

# dessa forma executará os métodos definidos no alias de cada modulo
obj.first_same_name_method
obj.second_same_name_method
```
