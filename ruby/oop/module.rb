# user class with destructable module:

module Destructable
  def destroy(anyobject)
    puts "I will destroy the object"
  end
end

class User
  include Destructable

  attr_accessor :name, :email

  def initialize(name, email)
    @name = name
    @email = email
  end

  def run
    puts "Hey I'm running"
  end

  def self.identify_yourself
    puts "Hey I am a class method"
  end
end

class Buyer < User
  def run
    puts "Hey I'm not running and I'm in buyer class"
  end
end

class Seller < User

end

class Admin < User

end

class MyClass
  attr_reader :readonly_attr # get
  attr_writer :writeonly_attr # set
  attr_accessor :public_attr # get e set

  def initialize
    @readonly_attr = 1
    @writeonly_attr = 2
    @public_attr = 3
    @private_attr = 4 # não estará visivel para ninguém
  end

  private
    def my_private_method
      puts "Método privado"
    end

  protected
    def my_protected_method
      puts "Método protegido"
    end

  public
    def my_public_method
      puts "Método público"
    end
end

my_obj = MyClass.new
puts my_obj.readonly_attr
puts my_obj.writeonly_attr # não funciona
puts my_obj.public_attr
puts my_obj.private_attr # não funciona

puts my_obj.my_private_method # não funciona
puts my_obj.my_protected_method # não funciona
puts my_obj.my_public_method
