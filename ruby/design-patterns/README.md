# Implementação dos design patterns em Ruby

* Prototype
* Builder
* Singleton
* Factory Method
* Proxy
* Decorator
* Facade
* Adapter
* Flyweight
* Chain of Responsibility
* Iterator
* State
* Strategy
* Observer
* Visitor
* Template Method
* Command
* Memento
* Mediator
