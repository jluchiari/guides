# Guia para o uso do Ruby on Rails

TODO:
* [ ] - Diretórios do Rails
* [ ] - Rake
* [ ] - Separar as seções em arquivos
* [ ] - Autenticação
* [ ] - API only
* [ ] - Testes
* [ ] - Docker
* [ ] - Mongoid
* [ ] - Jobs
* [ ] - Mailer
* [ ] - Revisar textos
* [ ] - Front end (task runner)

* [Rails](http://rubyonrails.org)
* [Guia oficial](http://guides.rubyonrails.org)
* [Rails style guide](https://github.com/rubensmabueno/ruby-style-guide/blob/master/README-PT-BR.md)

`rails console`: Inicia a linha de comando do rails
`reload!`: Recarrega a linha de comando

#### Rotas
Criação de rotas no arquivo `config/routes.rb`
```ruby
# declarando uma rota
get "rota" => "controller#method"

# declarando a rota principal
root "controller#method"
```

Para verificar todas as rotas cadastradas:
```bash
rake routes
```

Para criar um link com a rota na view
```ruby
<%= link_to "Home", root_path %>

<%= link_to "Outra rota", rota_path %>
```

O metodo de acesso ao `path` é dinâmico, portanto é substituido as `/` por `_`e adicionado `path` ao final.
Exemplo: se tivermos uma rota apontando para `/profile/me` o método para link ao path seria `profile_me_path`.


Para declarar todas as rotas de uma model
```ruby
resources :articles

#exceto alguns metodos
resources :users, except: [:new]
```




#### Gerando CRUD's
Para gerar um CRUD automáticamente, com as models, rotas, controllers, testes e tudo mais:
```bash
rails generate scaffold Article title:string description:text
```

Genericamente falando:
```bash
rails generate scaffold <Model> <attribute>:<type> <attribute>:<type>
```

Para destruir um CRUD:
```bash
rails destroy scaffold <Model>
```

Para criar um relacionamento:
```ruby
<attribute>:references
```

Para criar um migraiton:
```bash
rails generate migration create_articles
```

Após, será necessário ir no arquivo gerado em `db/migrate` e adicionar os atributos.

Para migrar o banco:
```bash
rake db:migrate
```

Volta para o último migration:
```bash
rake db:rollback
```

Para adicionar uma propriedate ao arquivo de migração, ao criar:
```ruby
t.<type> :<property_name>

# exemplo
t.string :title

# para adicionar o "created_at" e "updated_at"
t.timestamps

# criar um relacionamento
t.references :user, index: true, foreign_key: true
```

Para adicionar uma propriedate ao arquivo de migração, ao atualizar:
```ruby
add_column <table>, <property_name>, <type>
# ou
add_column :articles, :description, :text

# criar uma senha
add_column :users, :password_digest, :string
```


#### Models
As models ficam no diretório `app/models`.

Para criar uma model:
```ruby
class Article < ActiveRecord::Base

end
```
A model conterá todos os dados que estão na tabela.

Para criar um relacionamento 1 pra N:
```ruby
class Article < ActiveRecord::Base
  belongs_to :user # um artigo possui somente um usuário
end

class User < ActiveRecord::Base
  # tem que ser escrito no plural
  has_many :articles # um usuário possui N artigos
end
```

Para criar uma instância da model:
```ruby
article = Article.new
article.title = "This is the title"
article.description = "This is the description"

# ou

article = Article.new(title: "This is the title", description: "This is the description")
```

Para adicionar na tabela:
```ruby
article.save;
```

Para consultar todos os registros da model:
```ruby
Article.all
```

Para encontrar o primeiro e último registro da model:
```ruby
Article.first

Article.last
```

Para criar e já adicionar na tabela:
```ruby
Article.create(title: "This is the title", description: "This is the description")
```

Para buscar um registro:
```ruby
article = Article.find(1);
```

Para editar um registro:
```ruby
article = Article.find(1);
article.title = "Vou mudei"
article.save
```

Para remover um registro:
```ruby
article = Article.find(1);
article.destroy
```

Para que seja executado algo antes de salvar a model:
```ruby
before_save {
  # faça algo aqui!!!
}
```

Para adicionar uma senha a uma model de usuário:
```ruby
class User < ActiveRecord::Base
  has_secure_password
end
```

Para autenticar o usuário:
```ruby
User.find(42)
user.authenticate('password')
```

#### Validações
Dentro da model:
```ruby
validates :title, presence: true, length: { minimum: 3, maximum: 50 }
```

Alguns parâmetros para validação:
* `presence` - Recebe um booleano, fazendo com que o atributo se torne obrigatório ou não;
* `uniqueness` - Recebe um booleano, fazendo com que o atributo seja único, ou um hash, com a chave `case_sensitive` e o valor booleano, para que seja ou não diferenciado letras maiúsculas de minusculas;
* `length` - Recebe um hash, informando o tamanho mínimo (`minimum`) e/ou máximo (`maximum`), ambos recebem valores inteiros;
* `format` - Recebe um hash, com a chave `with`, informando a expressão regular;

Para validar a model:
```ruby
article.valid?
```

Para verificar se contém erros:
```ruby
article.error.any?
```

Para verificar se existem erros:
```ruby
article.error.full_messages
```

#### Controllers
As rotas sempre redirecionam para algo como `controller#action`

Quando se cria uma model, as rotas disponíveis são:
* `GET /articles`, redireciona para `articles#index` - É a rota principal da model
* `GET /articles/new`, redireciona para `articles#new` - Exibe o formulário de cadastro de um novo item da model
* `POST /articles`, redireciona para `articles#create` - Cria um novo item da model
* `GET /articles/:id`, redireciona para `articles#edit` - Exibe o formulário de edição de um item da model
* `PATCH /articles/:id`, redireciona para `articles#update` - Altera os dados de um item da model
* `GET /articles/:id`, redireciona para `articles#show` - Exibe os dados de um item da model
* `DELETE /articles/:id`, redireciona para `articles#destroy` - Remove um item da model

Para criar um controller (na pasta `app/controllers`), o nome do arquivo precisa ser algo como `article_controller.rb`:
```ruby
class ArticlesController < ApplicationController
  # defina todos os métodos padrões das rotas aqui

  # para pegar os dados da model que venham de uma requisição
  private
    def article_params
      params.require(:article).permit(:title, :description)
    end
end
```

A action `new` deverá ser parecida com isso:
```ruby
def new
  @article = Article.new
end
```

A action `create` deverá ser parecida com isso:
```ruby
def create
  @article = Article.new(article_params)
  if @article.save
    flash[:notice] = "Article was successfully created"
    redirect_to article_path(@article)
  else
    render 'new'
  end
end
```

A action `show` deverá ser parecida com isso:
```ruby
def show
  @article = Article.find(params[:id])
end
```

A action `edit` deverá ser parecida com isso:
```ruby
def edit
  @article = Article.find(params[:id])
end
```

A action `update` deverá ser parecida com isso:
```ruby
def update
  @article = Article.find(params[:id])
  if @article.update(article_params)
    flash[:notice] = "Article was successfully updated"
    redirect_to articles_path(@article)
  else
    render 'edit'
  end
end
```

A action `index` deverá ser parecida com isso:
```ruby
def index
  @articles = Article.all
end
```

A action `destroy` deverá ser parecida com isso:
```ruby
def destroy
  @article = Article.find(params[:id])
  @article.destroy
  flash[:notice] = "Article was successfully deleted"
  redirect_to articles_path
end
```

Para debugar o valor vindo de parametros no header de uma requisição:
```ruby
render plain: params[:article].inspect # debug
```

Para redirecionar o usuário a outra rota ao final de uma action:
```ruby
redirect_to route_path(@model)
# Exemplo
redirect_to article_path(@article)
```

Para criar uma mensagem para a view:
```ruby
flash[:notice] = "Sua mensagem de sucesso aqui!!!"
```

Para renderizar uma view:
```ruby
render 'new'
```

Para utilizar um método antes de uma action:
```ruby
before_action :method_to_execute_before, only: [:actions, :you, :want, :to, :do, :it]
```


#### Views
* [Form Helpers](http://guides.rubyonrails.org/form_helpers.html)

Para criar um formulário na view:
```html
<%= form_for @article do |f| %>
  <!-- Inputs e submit aqui... -->
<% end %>
```

Para criar um campo:
```html
<%= f.label :title %>
<%= f.text_field :title %>
```

Tipos de campos:
* `f.text_field` - Campo de texto normal (`<input type="text" />`)
* `f.text_area` - Cria um campo textarea (`<textarea></textarea>`)

Para criar o botão de submissão:
```html
<%= f.submit %>
```

Para exibir mensagens vindas do controller:
```html
<% flash.each do |name, msg| %>
  <%= msg %>
<% end %>
```

Para exibir erros nos formulários:
```html
<% if @article.errors.any? %>
  <h2>The following errors prevented the article from getting created</h2>
  <ul>
    <% @article.errors.full_messages.each do |msg| %>
      <li><%= msg %></li>
    <% end %>
  </ul>
<% end %>
```

Para criar um link:
```html
<%= link_to "Link name", route_path %>
<!-- Exemplo -->
<%= link_to "Create new article", new_article_path %>
```

Para criar um link com um método HTTP diferente:
```html
<%= link_to 'Delete', article_path(article), method: :delete, data: {confirm: "Are you sure?"} %>
```

Para criar uma partial view, basta criar com um underscore em frente ao arquivo, exemplo: `_form.html.erb`.

Para referenciar a partial view dentro de uma view:
```html
<%= render 'form' %>
<!-- caso a view esteja em outro path -->
<%= render 'layouts/outra_partial'
```

Para formatar uma data:
```ruby
time_ago_in_words(data)
```

Para verificar se a model está sendo criada ou atualizada:
```ruby
@article.new_record?
```

#### Pagination
Primeiro, instalar a gem:
```ruby
gem 'will_paginate'
```

Para dizer que os registros da model serão paginados:
```ruby
@articles = Article.paginate(page: params[:page], per_page: 5)
```

Na view:
```html
<%= will_paginate %>
```

Caso esteja paginando algo que está em um relacionamento de uma classe:
```ruby
@user = User.find(params[:id])
@user_articles = @user.articles.paginate(page: params[:page], per_page: 5)
```

E na view:
```html
<%= will_paginate @user_articles %>
```

#### Debug
Para exibir na view os parâmetros trafegados entre as requisições:
```html
<%= debug(params) if Rails.env.development? %>
```

Para debugar qualquer código no rails:
```ruby
debugger
```
Todo código comentado após a declaração do debugger, será considerado um breakpoint.

Para exibir os parametros recebidos em uma action:
```ruby
render plain: params[:article].inspect
```

#### Convenções
O arquivo da model precisa ser no estilo snake case (ex: `article.rb`), enquanto a classe, camel case (ex: `Article`) e por fim a tabela também snake case, no plural (ex: `articles`).
No caso do controller, precisa ser adicionado `_controller` no arquivo (ex: `article_controller.rb`).
Os arquivos das views ficarão no diretório `article`.
