# Bibliotecas úteis da comunidade Ruby

* [ ] - Inspecionar gem file do rails
* [ ] - O que é o gem
* [ ] - Como criar um gem
* [ ] - O que é o `Gemfile.lock`


Para utilizar uma biblioteca do ruby, utilizamos o ruby gems.
Para instalar uma gem, basta utilizar o comando `gem install <gem name>`. Será instalado globalmente, onde qualquer script ruby na máquina conseguira utilizar essa gem.

Para criar um `Gemfile`:
```ruby
source 'https://rubygems.org'

gem '<gem_name>'
```

Após, basta executar `bundle install` para instalar as gems.
Depois de rodar o comando, será criado um arquivo chamado `Gemfile.lock`, onde basicamente estão informações das gems que foram instaladas, como dependências e versões.


Para utilizar uma gem em um script do ruby:
```ruby
require 'gem_name'
```
