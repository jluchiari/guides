# Instalação e configuração do Ruby

### Rbenv
É necessário instalar alguns pacotes requeridos pelo rbenv:
```bash
sudo apt update
sudo apt install -y git autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev
```

Após, clone o repositório do rbenv:
```bash
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
```

Adicione o rbenv ao PATH:
```bash
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
source ~/.bashrc
```

Verifique a instalação:
```bash
type rbenv
```

Para poder manipular diferentes versões do ruby:
```bash
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
```

Para verificar todas as versões do ruby disponíveis para instalação:
```bash
rbenv install -l
```

Para instalar uma versão do ruby:
```bash
rbenv install 2.4.1
rbenv global 2.4.1 // para usar esta versão
```

Para verificar a versão do ruby instalada:
```bash
ruby -v
```

Remover uma versão do ruby:
```bash
rbenv uninstall 2.4.0
```

Para atualizar o rbenv:
```bash
cd ~/.rbenv
git pull
```

Não baixar documentação das gems:
```bash
echo "gem: --no-document" > ~/.gemrc
```

Instalar o bundler:
```bash
gem install bundler
```
