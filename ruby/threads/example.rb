require 'net/http'

def http_get(str_url)
  url = URI.parse(str_url)
  req = Net::HTTP::Get.new(url.to_s)

  Net::HTTP.start(url.host, url.port) { |http|
    http.request(req)
  }
end

100.times { |index|
  puts "#{ index }x =================== "

  threads = (0..25).collect { |i|
    Thread.new {
      response = http_get('http://www.pudim.com.br/')

      puts "Success" if response.code.to_i == 200
      puts "Fail" if response.code.to_i != 200
    }
  }

  threads.each { |t| t.join }
}
