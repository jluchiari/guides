# Programação parelela com Ruby

- [ ] Main thread
- [ ] Criando threads
- [ ] Sincronizando threads com `Fiber`
- [ ] Organizar seções
- [ ] Criar arquivos de exemplo

Sintaxe para criação de threads:
```ruby
t = Thread.new {
  # execute some code
}
```
_Nota: cuidado para não executar as threads depois que a main thread terminar de rodar_

Acesso a main thread:
```ruby
Thread.main
```

Exemplo de criação de threads:
```ruby
threads = (0..100).collect { |i|
  t = Thread.new {
    puts i
  }
}

threads.each { |t| t.join }
```

Sincronizando threads caso precise alterar valores de variáveis globais:
```ruby
$i = 0

semaphore = Mutex.new

threads = (0..2).collect {
  Thread.new {
    semaphore.synchronize {
      1_000_000.times { $i = $i + 1 }
    }
  }
}

threads.each { |t| t.join }

puts $i
```

Não façam isso em casa crianças...
```ruby
require 'net/http'

def http_get(str_url)
  url = URI.parse(str_url)
  req = Net::HTTP::Get.new(url.to_s)

  Net::HTTP.start(url.host, url.port) { |http|
    http.request(req)
  }
end

100.times { |index|
  puts "#{ index }x =================== "

  threads = (0..25).collect { |i|
    t = Thread.new {
      response = http_get('http://www.pudim.com.br/')

      puts "Success" if response.code.to_i == 200
      puts "Fail" if response.code.to_i != 200
    }
  }

  threads.each { |t| t.join }
}
```

Sintaxe fiber:
```ruby
f = Fiber.new do
  puts "In fiber"
  Fiber.yield("yielding")
  puts "Still in fiber"
  Fiber.yield("yielding again")
  puts "But still in fiber"
end

puts "a"
puts f.resume
puts "b"
puts f.resume
puts "c"
puts f.resume
puts "d"
puts f.resume # dead fiber called

# verifica se o fiber está disponível
if f.alive?
  puts f.resume
else
  puts "Error: Call to dead fiber"
end
```
