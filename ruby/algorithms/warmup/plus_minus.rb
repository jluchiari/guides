arr = [-4, 3, -9, 0, 4, 1]

c_positive = 0
c_negative = 0
c_zero = 0

arr.each do |n|
  res = n <=> 0

  c_positive += 1 if res > 0
  c_zero += 1 if res == 0
  c_negative += 1 if res < 0
end

puts c_positive.to_f / arr.length
puts c_negative.to_f / arr.length
puts c_zero.to_f / arr.length
