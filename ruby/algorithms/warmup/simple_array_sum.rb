def simpleArraySum(ar)
  total = 0
  ar.each { |elem| total += elem }
  total
end

ar = [1, 2, 3, 4, 10, 11]
result = simpleArraySum(ar)
puts result;
