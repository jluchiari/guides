n = 6

h_n = 1

while h_n <= n do
  s_n = n - h_n

  puts "#{ " " * s_n }#{ "#" * h_n }"

  h_n += 1
end
