require "date"

def timeConversion(s)
    time = DateTime.strptime(s, "%l:%M:%S%p")
    time.strftime("%H:%M:%S")
end

s = "07:05:45PM"
result = timeConversion(s)
puts result;
