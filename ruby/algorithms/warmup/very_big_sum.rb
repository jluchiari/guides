def aVeryBigSum(ar)
  total = 0
  ar.each { |elem| total += elem }
  total
end

ar = [1000000001, 1000000002, 1000000003, 1000000004, 1000000005]
result = aVeryBigSum(ar)
puts result;
