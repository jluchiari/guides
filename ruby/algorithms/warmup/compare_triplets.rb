def solve(a0, a1, a2, b0, b1, b2)
  result_a = 0
  result_b = 0

  result_a += 1 if a0 > b0
  result_b += 1 if a0 < b0

  result_a += 1 if a1 > b1
  result_b += 1 if a1 < b1

  result_a += 1 if a2 > b2
  result_b += 1 if a2 < b2

  result_arr = [result_a, result_b]
  result_arr
end

a0, a1, a2 = 5, 6, 7
b0, b1, b2 = 3, 6, 10

result = solve(a0, a1, a2, b0, b1, b2)
puts result.join(" ")
