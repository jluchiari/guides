def birthdayCakeCandles(ar)
  tallest = ar.max
  t_arr = ar.select { |n| n == tallest }
  t_arr.length
end

ar = [3, 2, 1, 3]
result = birthdayCakeCandles(ar)
puts result;
