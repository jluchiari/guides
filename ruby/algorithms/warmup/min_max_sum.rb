arr = [1, 2, 3, 4, 5]

i = 0
sums = []

arr.length.times do
  t_arr = arr.clone

  t_arr.delete_at(i)

  sums[i] = 0

  for n in t_arr
    sums[i] += n
  end

  i += 1
end

puts "#{sums.min} #{sums.max}"
