a = [
  [ 11, 2, 4 ],
  [ 4, 5, 6 ],
  [ 10, 8, -12 ]
]

diff = 0

a0 = 0
a1 = 0

a.each_with_index do |a_e, a_i|
  a0 += a_e[a_i]
  a1 += a_e[(a_i - (a.length - 1)).abs]
end

diff = a0 - a1

puts diff.abs
