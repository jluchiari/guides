def show_family (the_class)
  unless (the_class.nil?)
    puts "#{ the_class }"
    show_family(the_class.superclass)
  end
end

begin
  x = 1 / 0
rescue Exception
  x = 0
  puts $!.class
  puts $!

  show_family($!.class)
end

puts x
