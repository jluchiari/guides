# Tratamento de exceções em Ruby

- [ ] Tratando uma exceção
- [ ] Lançando uma exceção
- [ ] Organizar seções
- [ ] Criar arquivos de exemplo

Sintaxe para tratar excessões:
```ruby
begin
  # algum código que possa causar uma excessão
rescue Exception
  # código para tratar a excessão
end
```

Exemplo:
```ruby
begin
  x = 1 / 0
rescue Exception
  x = 0
  puts $!.class # classe da excessão
  puts $! # imprime o texto da excessão
end

puts x
```
_Nota: o `$!` é um caractere especial no qual o ruby deixa como valor a última excessão ocorrida_

Para mais de uma excessão e com alias e `retry`:
```ruby
begin
  # algum código que possa causar uma excessão
rescue TypeError, NoMethodError => e
  # código para tratar o erro
rescue Exception => e
  # código para tratar a excessão
  retry # tenta denovo executar o código
ensure
  # código a ser executado com excessão ou não
  # por exemplo fechar uma conexão com o banco ou um arquivo
end
```
