# Guia para remover a sintaxe do Java da cabeça

### Instalação

##### Ubuntu 16.04
Basta executar os seguintes comandos:
```bash
sudo apt update
sudo apt install -y default-jre default-jdk
```

### Frameworks
* [Spring](spring/)

### Pesquisar
* [ ] - Queue - [JMS]
* [ ] - ORM - [JPA]
* [ ] - ODM - [Morphia]
* [ ] - Query Language - [GraphQL]
* [ ] - Search engine - [Elasticsearch]
* [ ] - Test - [JUnit]
* [ ] - Test - [Mockito]
* [ ] - Test - [Cucumber](https://cucumber.io/docs/reference/jvm#java)
* [ ] - Test - [MockMvc]
* [ ] - Test - [Betamax]
* [ ] - Key/Value DB - [Redis]
* [ ] - Web Server - [Tomcat]
* [ ] - Dependency Manager - [Maven]
* [ ] - Dependency Manager - [Gradle]
