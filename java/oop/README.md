# Orientação a objetos em Java

### Classes, Interfaces, Herança e Polimorfismo
Declaração de classes:
```java
class MyClass {
  // métodos e atributos
}
```

Declaração de interfaces:
```java
interface MyInterface {
  // declaração de métodos e constantes
}
```

Classes abstratas:
```java
abstract class MyAbstractClass {
  // declaração de métodos e atributos
}
```

Uso de herança:
```java
class MyClass extends MyAbstractClass {
  // declaração de atributos, métodos e implementação de métodos abstratos
}
```

Uso de interfaces:
```java
class MyClass implements MyInterface {
  // declaração de atributos, métodos e implementação dos métodos da interface
}
```

### Atributos, Métodos e Encapsulamento
Declaração de um atributo:
```java
int myIntegerAttribute;
String myStringAttribute;
double myDoubleAttribute = 20.0; // default value

<DataType> <attributeName>;
```

Declaração de métodos:
```java
int myMethod() {
	// lógica
}

void myMethod(int myInt) {
	// lógica
}

<return type> <methodName>(arguments) {
  // lógica
}
```

Para encapsular métodos ou atributos:
```java
<private|protected|public> int myIntegerAttribute;

<private|protected|public> int myIntegerMethod() {
    // lógica
}
```

### Stream
Para ler conteúdo do escrito no console.

Declaração:
```java
Scanner scan = new Scanner(System.in);
```

Métodos comuns:
```java
scan.next(); // returns the next token of input
scan.hasNext(); // returns true if there is another token of input (false otherwise)
scan.nextLine() // returns the next LINE of input
scan.hasNextLine(); // returns true if there is another line of iput
````

Para fechar o stream:
```java
scan.close();
```

Uso simples:
```java
Scanner scan = new Scanner(System.in);
String s = scan.next();
scan.close();
System.out.println(s);
```
