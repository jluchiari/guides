# OrientDB

#### Docker
```bash
docker run -d --name orientdb -p 2424:2424 -p 2480:2480 -e ORIENTDB_ROOT_PASSWORD=root orientdb
```

#### Comandos
Para trabalhar com classes no orientdb
```
create class Person extends V
create class Company extends V
create class WorkAt extends E


create class Person
create class Lead extends V
create class Vehicle cluster 10
create class DataItem abstract
alter class Person name Individual
alter class Account addcluster EuropeA
alter class Car clusterselection round-robin
drop class Person
truncate class Lead
```

Para criar atributos no orientdb
```
create property Person.name string
create property Person.job string

create property Company.name string
```

Para inserir registros:
```
insert into Person (name, job) values ("Julio", "Programador"), ("Seu João", "Motorista de Ônibus")
insert into Company set name = "iColabora"
```

#### CLI
Para listar bancos de dados no console:
```bash
list databases
```
