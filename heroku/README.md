# Guia para a CLI do Heroku

* [Documentação oficial](https://devcenter.heroku.com/articles/heroku-cli)

#### Instalação

**Debian/Ubuntu**:
```bash
sudo add-apt-repository "deb https://cli-assets.heroku.com/branches/stable/apt ./"
curl -L https://cli-assets.heroku.com/apt/release.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install heroku
```

Para verificar a versão:
```bash
heroku -v
```

#### Configuração
Para logar no heroku pela CLI:
```bash
heroku login
```
Basta colocar suas credencias e estará autenticado no heroku.

Para adicionar uma public key:
```bash
heroku keys:add
```

Para alterar a nome do repositório:
```bash
heroku rename <outro nome>
```

#### Para criar uma aplicação
Entre no repositório de sua aplicação e digite:
```bash
heroku create
```

Para realizar um deploy pelo git:
```bash
heroku git:remote -a appname
git push heroku master
```

Para acessar a aplicação: `appname.herokuapp.com`

Para executar um comando remotamente:
```bash
heroku run <bash command>
```
