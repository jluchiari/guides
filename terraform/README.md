# Guia para uso do Terraform
O Terraform é uma ferramenta para construir, alterar e controlar a infraestrutura versionada de forma segura e eficiente. O Terraform pode gerenciar provedores de serviços existentes e populares, bem como soluções internas personalizadas.

* [Site oficial](https://www.terraform.io/)
* [AWS](aws/)
* [Heroku](heroku/)
* [Google Cloud](gcp/)

### Instalação

1. Baixe o terraform de acordo com seu sistema operacional [aqui](https://www.terraform.io/downloads.html)
2. Extraia o arquivo `.zip` e coloque em um lugar de sua preferência (`/opt/terraform` ou `C:\Program Files\terraform`)
3. Adicione o arquivo no PATH:
```bash
export PATH=$PATH:/opt/terraform/
```
4. Configurar o PATH no [Windows](https://stackoverflow.com/questions/1618280/where-can-i-set-path-to-make-exe-on-windows)


Após, basta verificar a versão do terraform instalada:
```bash
terraform -v
```

### CLI
Todo
